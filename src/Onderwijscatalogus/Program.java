package Onderwijscatalogus;

import org.restlet.Component;
import org.restlet.data.Protocol;

import Onderwijscatalogus.Routers.MainRouter;

public class Program
{
	public static void main(String[] args) throws Exception
	{
		Component component = new Component();
		component.getServers().add(Protocol.HTTP, 8182);

		component.getDefaultHost().attach(new MainRouter(component));
		
		component.start();
	}
}
