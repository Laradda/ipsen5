package Onderwijscatalogus.Resources;

import Onderwijscatalogus.DataAccess.DataQueries;
import Onderwijscatalogus.Models.Recht;
import com.google.gson.GsonBuilder;
import org.restlet.data.Form;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.ResourceException;

/**
 * Created by chrissmits on 21/06/14.
 * ${CLASS}
 */
public class RechtResource extends BaseResource {

    @Override
    protected Representation get() throws ResourceException {
        DataQueries dq = new DataQueries();
        Recht recht;
        try {
            Object value = getRequest().getAttributes().get("id");
            int recht_id = Integer.parseInt(value.toString());
            recht = dq.getRecht(recht_id);
        } catch (NumberFormatException ex) {
            return new StringRepresentation("{'error':"+ex.getMessage()+"}");
        }

        String json = "";
        GsonBuilder builder = new GsonBuilder();
        json = builder.create().toJson(recht);

        return new StringRepresentation(json);
    }

    @Override
    protected Representation post(Representation entity) throws ResourceException {
        Form form = new Form(getRequest().getEntity());

        Recht recht = new Recht();
        try {
            recht.setFields(form.getValuesMap());
        } catch (NoSuchFieldError ex) {
            return new StringRepresentation("{'error':"+ex.getMessage()+"}");
        }

        recht.save();

        GsonBuilder builder = new GsonBuilder();
        String json = builder.create().toJson(recht);

        return new StringRepresentation(json);
    }

    @Override
    protected Representation put(Representation entity) throws ResourceException {
        Form form = new Form(getRequest().getEntity());
        Object value = getRequest().getAttributes().get("id");

        DataQueries dq = new DataQueries();
        Recht recht;
        try {
            int recht_id = Integer.parseInt(value.toString());
            recht = dq.getRecht(recht_id);
        } catch (NumberFormatException ex) {
            return new StringRepresentation("{'error':"+ex.getMessage()+"}");
        }

        //fail safe
        if(recht == null) {
            return new StringRepresentation("{'error':'No right found with ID : "+value+"!'}");
        }

        recht.setFields(form.getValuesMap());
        recht.update();

        String json = "";
        GsonBuilder builder = new GsonBuilder();
        json = builder.create().toJson(recht);

        return new StringRepresentation(json);

    }

    @Override
    protected Representation delete() throws ResourceException {
        Object value = getRequest().getAttributes().get("id");

        Recht recht;
        try {
            int recht_id = Integer.parseInt(value.toString());
            recht = new DataQueries().getRecht(recht_id);
        } catch (NumberFormatException ex) {
            return new StringRepresentation("{'error':"+ex.getMessage()+"}");
        }

        //fail safe
        if(recht == null) {
            return new StringRepresentation("{'error':'No right found with ID : "+value+"!'}");
        }

        recht.delete();

        return new StringRepresentation("1");
    }
}
