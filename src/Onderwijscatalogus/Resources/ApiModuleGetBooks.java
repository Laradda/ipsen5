package Onderwijscatalogus.Resources;

import Onderwijscatalogus.DataAccess.DataQueries;
import Onderwijscatalogus.Models.Module;
import com.google.gson.GsonBuilder;

import java.util.List;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.ResourceException;

/**
 * Created by Thijs on 30-6-2014.
 */
public class ApiModuleGetBooks extends BaseResource {

    @Override
    protected Representation get() throws ResourceException
    {
        Object value = getRequest().getAttributes().get("id");

        List<Integer> module = null;
        try {
            int moduleID = Integer.parseInt(value.toString());
            module = new DataQueries().getModuleBooks(moduleID);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }

        GsonBuilder builder = new GsonBuilder();

        String json = builder.create().toJson(module);

        return new StringRepresentation(json);
    }

}
