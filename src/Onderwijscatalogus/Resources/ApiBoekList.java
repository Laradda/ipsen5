package Onderwijscatalogus.Resources;

import Onderwijscatalogus.Models.Boek;
import Onderwijscatalogus.DataAccess.DataQueries;
import com.google.gson.GsonBuilder;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.ResourceException;

import java.util.List;

/**
 * Created by Michiel on 19-6-2014.
 */
public class ApiBoekList extends BaseResource {

    @Override
    protected Representation get() throws ResourceException {

        List<Boek> books = null;

        books = new DataQueries().getAll(Boek.class);

        String json = null;

        if(books.size() > 0) {
            GsonBuilder builder = new GsonBuilder();
            builder.setExclusionStrategies(books.get(0));
            json = builder.create().toJson(books);
        } else {
            json = "[empty]";
        }

        return new StringRepresentation(json);
    }
}
