package Onderwijscatalogus.Resources;

import Onderwijscatalogus.DataAccess.DataQueries;
import Onderwijscatalogus.Models.Module;
import com.google.gson.GsonBuilder;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.ResourceException;
import java.util.List;

/**
 * Created by Thijs on 19-6-2014.
 */
public class ApiModuleList extends BaseResource {
    @Override
    protected Representation get() throws ResourceException
    {

        List<Module> module = null;
        try {

            module = new DataQueries().getAll(Module.class);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
        String json = "[]";
        if(module.size() > 0) {
            GsonBuilder builder = new GsonBuilder();
            builder.setExclusionStrategies(module.get(0));
            json = builder.create().toJson(module);
        }
        return new StringRepresentation(json);
    }
}
