package Onderwijscatalogus.Resources;

import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.ResourceException;
import org.restlet.resource.ServerResource;

public class TestResource extends BaseResource
{
	public TestResource()
	{

	}

	@Override
	protected Representation get() throws ResourceException
	{
		Object user = getUser();
		return new StringRepresentation("TEST");
	}

}
