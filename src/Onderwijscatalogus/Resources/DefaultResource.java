package Onderwijscatalogus.Resources;

import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.ResourceException;

public class DefaultResource extends BaseResource
{
	public DefaultResource() {

	}
	
	@Override
	protected Representation get() throws ResourceException
	{
		Object user = getUser();

        return new StringRepresentation("Hello World");
	}
}
