package Onderwijscatalogus.Resources;

import Onderwijscatalogus.DataAccess.DataQueries;
import Onderwijscatalogus.Models.Docent;
import Onderwijscatalogus.Models.Gebruiker;
import com.google.gson.GsonBuilder;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.ResourceException;

/**
 * Created by Thijs on 27-6-2014.
 */
public class ApiLogin extends BaseResource
{
    @Override
    protected Representation get() throws ResourceException
    {
        String email = getQuery().getValues("email");
        String password = getQuery().getValues("password");

        String key = null;
        try {

            key = new DataQueries().generateKey(email,password);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }

        return new StringRepresentation(key);
    }
}
