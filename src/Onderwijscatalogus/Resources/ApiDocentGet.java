package Onderwijscatalogus.Resources;

import Onderwijscatalogus.DataAccess.DataQueries;
import Onderwijscatalogus.Models.Docent;
import com.google.gson.GsonBuilder;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.ResourceException;

/**
 * Created by Thijs on 21-6-2014.
 */
public class ApiDocentGet extends BaseResource {
    @Override
    protected Representation get() throws ResourceException
    {
        Object value = getRequest().getAttributes().get("id");

        Docent docent = null;
        try {
            int docentID = Integer.parseInt(value.toString());
            docent = new DataQueries().getDocent(docentID);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }

        GsonBuilder builder = new GsonBuilder();
        builder.setExclusionStrategies(docent);
        String json = builder.create().toJson(docent);

        return new StringRepresentation(json);
    }
}
