package Onderwijscatalogus.Resources;

import Onderwijscatalogus.DataAccess.DataQueries;
import Onderwijscatalogus.Models.Module;
import com.google.gson.GsonBuilder;
import org.restlet.data.Form;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.Post;
import org.restlet.resource.ResourceException;

/**
 * Created by Thijs on 19-6-2014.
 */
public class ApiModuleEdit extends BaseResource {

    @Post("form:json")
    @Override
    public Representation post(Representation representation) throws ResourceException
    {
        Form form = new Form(getRequest().getEntity());

        Object value = getRequest().getAttributes().get("id");

        Module module = null;
        try {
            int moduleID = Integer.parseInt(value.toString());
            module = new DataQueries().getModule(moduleID);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
        module.setFields(form.getValuesMap());

        module.update();

        GsonBuilder builder = new GsonBuilder();
        builder.setExclusionStrategies(module);
        String json = builder.create().toJson(module);

        return new StringRepresentation(json);
    }
}
