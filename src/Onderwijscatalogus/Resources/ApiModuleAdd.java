package Onderwijscatalogus.Resources;

import Onderwijscatalogus.Models.Module;
import com.google.gson.GsonBuilder;
import org.restlet.data.Form;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.Post;
import org.restlet.resource.ResourceException;

/**
 * Created by Thijs on 13-6-2014.
 */
public class ApiModuleAdd  extends BaseResource {

    @Post
    @Override
    public Representation post(Representation representation) throws ResourceException
    {

        Form form = new Form(getRequest().getEntity());

        Module module = new Module();
        module.setFields(form.getValuesMap());

        module.save();

        GsonBuilder builder = new GsonBuilder();
        builder.setExclusionStrategies(module);
        String json = builder.create().toJson(module);

        return new StringRepresentation(json);
    }
}
