package Onderwijscatalogus.Resources;

import Onderwijscatalogus.DataAccess.DataQueries;
import Onderwijscatalogus.Models.RechtenGroep;
import com.google.gson.GsonBuilder;
import org.restlet.data.Form;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.ResourceException;

/**
 * Created by chrissmits on 21/06/14.
 * ${CLASS}
 */
public class RechtenGroepResource extends BaseResource {

    @Override
    protected Representation get() throws ResourceException {
        DataQueries queries = new DataQueries();
        Object value = getRequest().getAttributes().get("id");

        RechtenGroep recht_groep;
        try {
            int groep_id = Integer.parseInt(value.toString());
            recht_groep = new DataQueries().getGroep(groep_id);
        } catch (NumberFormatException ex) {
            return new StringRepresentation("{'error':"+ex.getMessage()+"}");
        }

        String json = "";
        GsonBuilder builder = new GsonBuilder();
        json = builder.create().toJson(recht_groep);

        return new StringRepresentation(json);
    }

    @Override
    protected Representation post(Representation entity) throws ResourceException {
        Form form = new Form(getRequest().getEntity());

        RechtenGroep recht_groep = new RechtenGroep();
        try {
            recht_groep.setFields(form.getValuesMap());
        } catch (NoSuchFieldError ex) {
            return new StringRepresentation("{'error':"+ex.getMessage()+"}");
        }

        recht_groep.save();

        GsonBuilder builder = new GsonBuilder();
        String json = builder.create().toJson(recht_groep);

        return new StringRepresentation(json);
    }

    @Override
    protected Representation put(Representation entity) throws ResourceException {
        Form form = new Form(getRequest().getEntity());
        Object value = getRequest().getAttributes().get("id");

        RechtenGroep recht_groep;
        try {
            int groep_id = Integer.parseInt(value.toString());
            recht_groep = new DataQueries().getGroep(groep_id);
        } catch (NumberFormatException ex) {
            return new StringRepresentation("{'error':"+ex.getMessage()+"}");
        }

        //fail safe
        if(recht_groep == null) {
            return new StringRepresentation("{'error':'No right group found with ID : "+value+"!'}");
        }

        recht_groep.setFields(form.getValuesMap());
        recht_groep.update();

        GsonBuilder builder = new GsonBuilder();
        String json = builder.create().toJson(recht_groep);

        return new StringRepresentation(json);
    }

    @Override
    protected Representation delete() throws ResourceException {
        Object value = getRequest().getAttributes().get("id");

        RechtenGroep recht_groep;
        try {
            int groep_id = Integer.parseInt(value.toString());
            recht_groep = new DataQueries().getGroep(groep_id);
        } catch (NumberFormatException ex) {
            return new StringRepresentation("{'error':"+ex.getMessage()+"}");
        }

        //fail safe
        if(recht_groep == null) {
            return new StringRepresentation("{'error':'No right group found with ID : "+value+"!'}");
        }

        recht_groep.delete();

        return new StringRepresentation("1");
    }
}
