package Onderwijscatalogus.Resources;

import Onderwijscatalogus.DataAccess.DataQueries;
import Onderwijscatalogus.Models.Docent;
import com.google.gson.GsonBuilder;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.ResourceException;

import java.util.List;

/**
 * Created by Thijs on 21-6-2014.
 */
public class ApiDocentList extends BaseResource {
    @Override
    protected Representation get() throws ResourceException
    {

        List<Docent> docent = null;
        try {

            docent = new DataQueries().getAll(Docent.class);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
        String json = "[]";
        if(docent.size() > 0) {
            GsonBuilder builder = new GsonBuilder();
            builder.setExclusionStrategies(docent.get(0));
            json = builder.create().toJson(docent);
        }
        return new StringRepresentation(json);
    }
}
