package Onderwijscatalogus.Resources;

import Onderwijscatalogus.Models.Boek;
import Onderwijscatalogus.DataAccess.DataQueries;
import com.google.gson.GsonBuilder;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.ResourceException;

/**
 * Created by Michiel on 19-6-2014.
 */
public class ApiBoekGetISBN extends BaseResource {

    @Override
    protected Representation get() throws ResourceException {

        Object value = getRequest().getAttributes().get("isbn");

        Boek boek = new DataQueries().getBoekByIsbn(value.toString());

        GsonBuilder builder = new GsonBuilder();
        builder.setExclusionStrategies(boek);

        String result = builder.create().toJson(boek);
        return new StringRepresentation(result);
    }
}
