package Onderwijscatalogus.Resources;

import Onderwijscatalogus.DataAccess.DataQueries;
import Onderwijscatalogus.Models.Docent;
import com.google.gson.GsonBuilder;
import org.restlet.data.Form;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.ResourceException;

/**
 * Created by reshadfarid on 28-08-14.
 */
public class ApiModuleAddBoek extends BaseResource {

    public Representation post(Representation representation) throws ResourceException
    {
        Object module_value = getRequest().getAttributes().get("module_id");
        Object boek_value = getRequest().getAttributes().get("boek_id");

        try {
            int moduleId = Integer.parseInt(module_value.toString());
            int boekId = Integer.parseInt(boek_value.toString());

            int koppeling = new DataQueries().addBoekToModule(moduleId, boekId);

            return new StringRepresentation(koppeling + "");
        } catch (NumberFormatException ex) {
            System.out.println(ex.getMessage());
            return new StringRepresentation("0");
        }catch (NullPointerException ex) {
            // no need to log because this is not really an error
            return new StringRepresentation("0");
        }

    }

}
