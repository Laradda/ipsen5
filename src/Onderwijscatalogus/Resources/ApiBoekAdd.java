package Onderwijscatalogus.Resources;

import Onderwijscatalogus.Models.Boek;
import com.google.gson.GsonBuilder;
import org.restlet.data.Form;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.Post;
import org.restlet.resource.ResourceException;

/**
 * Created by Michiel on 20-6-2014.
 */
public class ApiBoekAdd extends BaseResource {

    @Post
    @Override
    public Representation put(Representation entity) throws ResourceException {

        Form form = new Form(getRequest().getEntity());

        Boek boek = new Boek();
        boek.setFields(form.getValuesMap());

        boek.save();

        GsonBuilder builder = new GsonBuilder();
        builder.setExclusionStrategies(boek);
        String json = builder.create().toJson(boek);

        return new StringRepresentation(json);
    }
}
