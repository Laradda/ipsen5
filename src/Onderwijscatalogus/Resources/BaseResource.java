package Onderwijscatalogus.Resources;

import java.util.concurrent.ConcurrentMap;

import org.restlet.Context;
import org.restlet.data.Form;
import org.restlet.resource.ServerResource;

import Onderwijscatalogus.Models.Gebruiker;
import Onderwijscatalogus.Routers.MainRouter;

public class BaseResource extends ServerResource
{
    protected void setHeaders() {
        Form responseHeaders = (Form) getResponse().getAttributes().get("org.restlet.http.headers");
        if (responseHeaders == null) {
            responseHeaders = new Form();
            getResponse().getAttributes().put("org.restlet.http.headers", responseHeaders);
        }
        responseHeaders.add("Access-Control-Allow-Origin", "http://sand.box");
    }

	protected Gebruiker getUser()
	{
		Context context = getContext();
		ConcurrentMap<String,Object> attributes = context.getAttributes();
		if(attributes.containsKey(MainRouter.USER_ATTRIBUTE_KEY))
		{
			return (Gebruiker)attributes.get(MainRouter.USER_ATTRIBUTE_KEY);
		}
		return null;
	}
}
