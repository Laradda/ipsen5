package Onderwijscatalogus.Resources;

import Onderwijscatalogus.DataAccess.DataQueries;
import Onderwijscatalogus.Models.Docent;
import com.google.gson.GsonBuilder;
import org.restlet.data.Form;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.Post;
import org.restlet.resource.ResourceException;

/**
 * Created by Thijs on 21-6-2014.
 */
public class ApiDocentEdit extends BaseResource {

    @Post("form:json")
    @Override
    public Representation post(Representation representation) throws ResourceException
    {
        Form form = new Form(getRequest().getEntity());

        Object value = getRequest().getAttributes().get("id");

        Docent docent = null;
        try {
            int docentID = Integer.parseInt(value.toString());
            docent = new DataQueries().getDocent(docentID);
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
        docent.setFields(form.getValuesMap());

        docent.update();

        GsonBuilder builder = new GsonBuilder();
        builder.setExclusionStrategies(docent);
        String json = builder.create().toJson(docent);

        return new StringRepresentation(json);
    }
}
