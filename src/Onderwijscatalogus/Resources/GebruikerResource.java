package Onderwijscatalogus.Resources;

import Onderwijscatalogus.DataAccess.DataQueries;
import Onderwijscatalogus.Models.Gebruiker;
import com.google.gson.GsonBuilder;
import org.restlet.data.Form;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.ResourceException;

import java.math.BigInteger;
import java.security.SecureRandom;

/**
 * Created by Chris
 */
public class GebruikerResource extends BaseResource {

    private SecureRandom random = new SecureRandom();

    @Override
    protected Representation get() throws ResourceException
    {
        DataQueries queries = new DataQueries();
        Gebruiker gebruiker;
        try {
            Object value = getRequest().getAttributes().get("id");
            int gebruiker_id = Integer.parseInt(value.toString());
            gebruiker = queries.getGebruiker(gebruiker_id);
        } catch (NumberFormatException ex) {
            return new StringRepresentation("{'error':"+ex.getMessage()+"}");
        }

        String json = "";
        GsonBuilder builder = new GsonBuilder();
        json = builder.create().toJson(gebruiker);

        return new StringRepresentation(json);
    }

    /**
     * Method to create an user
     *
     * @param entity json representation
     * @return success message
     * @throws ResourceException
     */
    @Override
    protected Representation post(Representation entity) throws ResourceException {
        Form form = new Form(getRequest().getEntity());

        Gebruiker gebruiker = new Gebruiker();
        try {
            gebruiker.setFields(form.getValuesMap());
        } catch (NoSuchFieldError ex) {
            return new StringRepresentation("{'error':"+ex.getMessage()+"}");
        }

        //TODO check if required fields are available
        System.out.println(gebruiker.getEmail());
        if(gebruiker.getEmail() == null) {
            return new StringRepresentation("{'error':'No email specified!'}");
        }

        gebruiker.setId(0);
        gebruiker.setRechtenGroep(1);
        gebruiker.setWachtwoord(new BigInteger(65, random).toString(32));
        gebruiker.setApikey(null);
        gebruiker.setApikeycreated(null);
        gebruiker.save();

        GsonBuilder builder = new GsonBuilder();
        String json = builder.create().toJson(gebruiker);

        return new StringRepresentation(json);
    }

    @Override
    protected Representation put(Representation entity) throws ResourceException {
        Form form = new Form(getRequest().getEntity());
        Object value = getRequest().getAttributes().get("id");

        Gebruiker gebruiker;
        try {
            int gebruiker_id = Integer.parseInt(value.toString());
            gebruiker = new DataQueries().getGebruiker(gebruiker_id);
        } catch (NumberFormatException ex) {
            return new StringRepresentation("{'error':"+ex.getMessage()+"}");
        }

        //fail safe
        if(gebruiker == null) {
            return new StringRepresentation("{'error':'No user found with ID : "+value+"!'}");
        }

        gebruiker.setFields(form.getValuesMap());
        gebruiker.update();

        GsonBuilder builder = new GsonBuilder();
        String json = builder.create().toJson(gebruiker);

        return new StringRepresentation(json);
    }

    @Override
    protected Representation delete() throws ResourceException {
        Object value = getRequest().getAttributes().get("id");

        Gebruiker gebruiker;

        try {
            int gebruiker_id = Integer.parseInt(value.toString());
            gebruiker = new DataQueries().getGebruiker(gebruiker_id);
        } catch (NumberFormatException ex) {
            return new StringRepresentation("{'error':"+ex.getMessage()+"}");
        }

        //fail safe
        if(gebruiker == null) {
            return new StringRepresentation("{'error':'No user found with ID : "+value+"!'}");
        }

        gebruiker.delete();

        return new StringRepresentation("1");
    }
}
