package Onderwijscatalogus.Resources;

import Onderwijscatalogus.Models.Boek;
import Onderwijscatalogus.DataAccess.DataQueries;
import com.google.gson.GsonBuilder;
import org.restlet.data.Form;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.Post;
import org.restlet.resource.ResourceException;

/**
 * Created by Michiel on 20-6-2014.
 */
public class ApiBoekEdit extends BaseResource {

    @Post("form:json")
    @Override
    public Representation post(Representation entity) throws ResourceException {

        Form form = new Form(getRequest().getEntity());
        Object value = getRequest().getAttributes().get("id");

        Boek boek = null;

        try {
            int boekId = Integer.parseInt(value.toString());
            boek = new DataQueries().getBoek(boekId);
        } catch (NumberFormatException ex) {

        }

        boek.setFields(form.getValuesMap());
        boek.update();

        GsonBuilder builder = new GsonBuilder();
        builder.setExclusionStrategies(boek);

        String result = builder.create().toJson(boek);
        return new StringRepresentation(result);
    }
}
