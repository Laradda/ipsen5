package Onderwijscatalogus.Resources;

import Onderwijscatalogus.DataAccess.DataQueries;
import Onderwijscatalogus.Models.Docent;
import Onderwijscatalogus.Models.Module;
import com.google.gson.GsonBuilder;

import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.ResourceException;

/**
 * Created by Thijs on 29-6-2014.
 */
public class ApiDocentGetModules extends BaseResource {
    @Override
    protected Representation get() throws ResourceException
    {
        Object value = getRequest().getAttributes().get("id");

        java.util.List<Integer> modules = null;
        try {
            int docentID = Integer.parseInt(value.toString());

            modules = new DataQueries().getModulesByDocent(docentID);

        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }

        if(modules != null && modules.size() > 0)
        {
            GsonBuilder builder = new GsonBuilder();

            String json = builder.create().toJson(modules);
            return new StringRepresentation(json);
        }


        return new StringRepresentation("[]");

    }
}
