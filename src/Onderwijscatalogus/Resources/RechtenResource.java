package Onderwijscatalogus.Resources;

import Onderwijscatalogus.DataAccess.DataQueries;
import Onderwijscatalogus.Models.Recht;
import Onderwijscatalogus.Models.RechtInGroep;
import com.google.gson.GsonBuilder;
import org.restlet.data.Form;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.ResourceException;

import java.util.List;

/**
 *
 */
public class RechtenResource extends BaseResource {

    @Override
    protected Representation get() throws ResourceException
    {
        DataQueries queries = new DataQueries();
        List<Recht> rechten;
        try {
            Object value = getRequest().getAttributes().get("id");
            int groep_id = Integer.parseInt(value.toString());
            rechten = queries.getRechtenGroep(groep_id);
            System.out.println(rechten);

        } catch (NumberFormatException ex) {
            return new StringRepresentation("{'error':"+ex.getMessage()+"}");
        }

        String json = "";
        GsonBuilder builder = new GsonBuilder();
        json = builder.create().toJson(rechten);

        return new StringRepresentation(json);
    }

    @Override
    protected Representation post(Representation entity) throws ResourceException {
        Form form = new Form(getRequest().getEntity());

        RechtInGroep recht_in_groep = new RechtInGroep();
        try {
            recht_in_groep.setFields(form.getValuesMap());
        } catch (NoSuchFieldError ex) {
            return new StringRepresentation("{'error':"+ex.getMessage()+"}");
        }

        recht_in_groep.save();

        GsonBuilder builder = new GsonBuilder();
        String json = builder.create().toJson(recht_in_groep);

        return new StringRepresentation(json);
    }

    @Override
    protected Representation delete() throws ResourceException {
        Object value = getRequest().getAttributes().get("id");

        RechtInGroep recht_in_groep;
        try {
            int groep_id = Integer.parseInt(value.toString());
            recht_in_groep = new DataQueries().getGroepRecht(groep_id);
        } catch (NumberFormatException ex) {
            return new StringRepresentation("{'error':"+ex.getMessage()+"}");
        }

        //fail safe
        if(recht_in_groep == null) {
            return new StringRepresentation("{'error':'No right group found with ID : "+value+"!'}");
        }

        recht_in_groep.delete();

        return new StringRepresentation("1");
    }
}
