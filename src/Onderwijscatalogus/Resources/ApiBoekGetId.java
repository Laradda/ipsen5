package Onderwijscatalogus.Resources;

import Onderwijscatalogus.Models.Boek;
import Onderwijscatalogus.DataAccess.DataQueries;
import com.google.gson.GsonBuilder;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.ResourceException;

/**
 * Created by Michiel on 19-6-2014.
 */
public class ApiBoekGetId extends BaseResource {

    @Override
    protected Representation get() throws ResourceException {

        Object value = getRequest().getAttributes().get("id");

        Boek book = null;

        try {
            int id = Integer.parseInt(value.toString());
            book = new DataQueries().getBoek(id);
        } catch(NumberFormatException ex) {

        }

        GsonBuilder builder = new GsonBuilder();
        builder.setExclusionStrategies(book);
        String result = builder.create().toJson(book);
        return new StringRepresentation(result);
    }
}
