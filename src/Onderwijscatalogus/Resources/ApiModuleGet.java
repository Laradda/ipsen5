package Onderwijscatalogus.Resources;

import Onderwijscatalogus.DataAccess.DataQueries;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.ResourceException;
import Onderwijscatalogus.Models.Module;
import com.google.gson.GsonBuilder;

public class ApiModuleGet extends BaseResource
{
	@Override
	protected Representation get() throws ResourceException
	{
		Object value = getRequest().getAttributes().get("id");

		Module module = null;
		try {
			int moduleID = Integer.parseInt(value.toString());
            module = new DataQueries().getModule(moduleID);
		} catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }

		GsonBuilder builder = new GsonBuilder();
        builder.setExclusionStrategies(module);
		String json = builder.create().toJson(module);

		return new StringRepresentation(json);
	}

}
