package Onderwijscatalogus.Resources;

import Onderwijscatalogus.Models.Boek;
import Onderwijscatalogus.DataAccess.DataQueries;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.ResourceException;

/**
 * Created by Michiel on 20-6-2014.
 */
public class ApiBoekDelete extends BaseResource {

    @Override
    protected Representation delete() throws ResourceException {

        Object value = getRequest().getAttributes().get("id");

        Boek boek = null;

        try {
            int boekId = Integer.parseInt(value.toString());
            boek = new DataQueries().getBoek(boekId);
            boek.delete();
            return new StringRepresentation("1");
        } catch (NumberFormatException ex) {
            System.out.println(ex.getMessage());
            return new StringRepresentation("0");
        }catch (NullPointerException ex) {
            // no need to log because this is not really an error
            return new StringRepresentation("0");
        }




    }
}
