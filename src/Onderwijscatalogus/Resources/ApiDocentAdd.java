package Onderwijscatalogus.Resources;

import Onderwijscatalogus.Models.Docent;
import com.google.gson.GsonBuilder;
import org.restlet.data.Form;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.Post;
import org.restlet.resource.ResourceException;

/**
 * Created by Thijs on 21-6-2014.
 */
public class ApiDocentAdd extends BaseResource {
    @Post
    @Override
    public Representation post(Representation representation) throws ResourceException
    {

        Form form = new Form(getRequest().getEntity());

        Docent docent = new Docent();
        docent.setFields(form.getValuesMap());

        docent.save();

        GsonBuilder builder = new GsonBuilder();
        builder.setExclusionStrategies(docent);
        String json = builder.create().toJson(docent);

        return new StringRepresentation(json);
    }
}