package Onderwijscatalogus.Resources;

import Onderwijscatalogus.DataAccess.DataQueries;
import Onderwijscatalogus.Models.Docent;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.ResourceException;

/**
 * Created by Thijs on 21-6-2014.
 */
public class ApiDocentDelete extends BaseResource {
    @Override
    protected Representation delete() throws ResourceException
    {
        Object value = getRequest().getAttributes().get("id");

        Docent docent = null;
        try {
            int docentID = Integer.parseInt(value.toString());
            docent = new DataQueries().getDocent(docentID);
            docent.delete();
            return new StringRepresentation("1");
        } catch (NumberFormatException ex) {
            System.out.println(ex.getMessage());
            return new StringRepresentation("0");
        }catch (NullPointerException ex) {
            // no need to log because this is not really an error
            return new StringRepresentation("0");
        }




    }
}
