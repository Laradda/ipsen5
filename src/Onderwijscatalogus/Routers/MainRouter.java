package Onderwijscatalogus.Routers;

import Onderwijscatalogus.Resources.*;

import Onderwijscatalogus.Security.AuthenticationRole;
import Onderwijscatalogus.Security.MainAuthenticator;
import org.restlet.Component;
import org.restlet.data.ChallengeScheme;
import org.restlet.data.Method;
import org.restlet.resource.ServerResource;
import org.restlet.routing.Router;

/**
 *
 * Edited by: Thijs vd Berg
              Michiel vd Kwartel
              Reshad Farid
 */
public class MainRouter extends Router
{
	public static final String USER_ATTRIBUTE_KEY = "user";

	public MainRouter(Component component)
	{
		this.setContext(component.getContext().createChildContext());
		attach("/", DefaultResource.class, new AuthenticationRole(Method.GET));

        attach("/login", ApiLogin.class, new AuthenticationRole( Method.GET));

        attach("/module/{module_id}/boek/{boek_id}/delete", ApiModuleDeleteBoek.class, new AuthenticationRole( Method.DELETE, "MODULE_DELETE_BOEK"));
        attach("/module/{module_id}/boek/{boek_id}", ApiModuleAddBoek.class, new AuthenticationRole( Method.POST, "MODULE_ADD_BOEK"));

        //user routes
        attach("/user/create", GebruikerResource.class, new AuthenticationRole( Method.POST,"USER_CREATE"));
        attach("/user/{id}", GebruikerResource.class, new AuthenticationRole( Method.GET));
        attach("/user/edit/{id}", GebruikerResource.class, new AuthenticationRole( Method.PUT,"USER_UPDATE"));
        attach("/user/delete/{id}", GebruikerResource.class, new AuthenticationRole( Method.DELETE,"USER_DELETE"));

        //rechten voor groep routes
        attach("/rechten/create", RechtenResource.class, new AuthenticationRole( Method.POST));
        attach("/rechten/{id}", RechtenResource.class, new AuthenticationRole( Method.GET));
        attach("/rechten/delete/{id}", RechtenResource.class, new AuthenticationRole( Method.DELETE));

        //rechten
        attach("/recht/create", RechtResource.class, new AuthenticationRole( Method.POST,"RECHT_CREATE"));
        attach("/recht/{id}", RechtResource.class, new AuthenticationRole( Method.GET,"RECHT_READ"));
        attach("/recht/edit/{id}", RechtResource.class, new AuthenticationRole( Method.PUT,"RECHT_EDIT"));
        attach("/recht/delete/{id}", RechtResource.class, new AuthenticationRole( Method.DELETE,"RECHT_DELETE"));

        //rechten groep
        attach("/groep/create", RechtenGroepResource.class, new AuthenticationRole( Method.POST,"RECHTENGROEP_CREATE"));
        attach("/groep/{id}", RechtenGroepResource.class, new AuthenticationRole( Method.GET,"RECHTENGROEP_READ"));
        attach("/groep/edit/{id}", RechtenGroepResource.class, new AuthenticationRole( Method.PUT,"RECHTENGROEP_UPDATE"));
        attach("/groep/delete/{id}", RechtenGroepResource.class, new AuthenticationRole( Method.DELETE,"RECHTENGROEP_DELETE"));

        attach("/module/list", ApiModuleList.class, new AuthenticationRole( Method.GET));
        attach("/module/add", ApiModuleAdd.class, new AuthenticationRole( Method.POST,"MODULE_CREATE"));
        attach("/module/delete/{id}", ApiModuleDelete.class, new AuthenticationRole( Method.DELETE,"MODULE_DELETE"));
        attach("/module/edit/{id}", ApiModuleEdit.class, new AuthenticationRole( Method.POST,"MODULE_UPDATE"));
        attach("/module/{id}/boeken",ApiModuleGetBooks.class,new AuthenticationRole(Method.GET));
        attach("/module/{id}", ApiModuleGet.class, new AuthenticationRole( Method.GET));

        attach("/docent/list", ApiDocentList.class, new AuthenticationRole( Method.GET));
        attach("/docent/add", ApiDocentAdd.class, new AuthenticationRole( Method.POST,"DOCENT_CREATE"));
        attach("/docent/delete/{id}", ApiDocentDelete.class, new AuthenticationRole( Method.DELETE,"DOCENT_DELETE"));
        attach("/docent/edit/{id}", ApiDocentEdit.class, new AuthenticationRole( Method.POST,"DOCENT_UPDATE"));
        attach("/docent/{id}/modules",ApiDocentGetModules.class,new AuthenticationRole(Method.GET));
        attach("/docent/{id}", ApiDocentGet.class, new AuthenticationRole( Method.GET));

        attach("/boek/list", ApiBoekList.class, new AuthenticationRole( Method.GET));
        attach("/boek/add", ApiBoekAdd.class, new AuthenticationRole( Method.POST,"BOEK_CREATE"));
        attach("/boek/delete/{id}", ApiBoekDelete.class, new AuthenticationRole( Method.DELETE,"BOEK_DELETE"));
        attach("/boek/edit/{id}", ApiBoekEdit.class, new AuthenticationRole( Method.POST,"BOEK_UPDATE"));
        attach("/boek/isbn/{isbn}", ApiBoekGetISBN.class, new AuthenticationRole( Method.GET));
        attach("/boek/{id}", ApiBoekGetId.class, new AuthenticationRole( Method.GET));

	}

	private void attach(String path, Class<? extends ServerResource> resource, AuthenticationRole role)
	{
		MainAuthenticator authenticator = new MainAuthenticator(this, ChallengeScheme.HTTP_BASIC, role);

		authenticator.setNext(resource);

		attach(path, authenticator);
	}

	public void setUser(Object user)
	{
		getContext().getAttributes().put(USER_ATTRIBUTE_KEY,user);
	}
}
