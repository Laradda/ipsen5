package Onderwijscatalogus.TestCases; /**
 * Created with IntelliJ IDEA.
 * User: reshadfarid
 * Date: 21-05-14
 * Time: 13:17
 * To change this template use File | Settings | File Templates.
 */

import Onderwijscatalogus.Models.Gebruiker;
import junit.framework.TestCase;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.util.List;

public class Gebruiker_test extends TestCase {

    private SessionFactory sessionFactory;

    @Override
    protected void setUp() throws Exception {
        // A SessionFactory is set up once for an application
        sessionFactory = new Configuration()
                .configure() // configures settings from hibernate.cfg.xml
                .buildSessionFactory();
    }

    @Override
    protected void tearDown() throws Exception {
        if ( sessionFactory != null ) {
            sessionFactory.close();
        }
    }

    public void testBasicUsage() {

        Gebruiker r = new Gebruiker();
        r.setEmail("info@thijs.nl");
        r.setWachtwoord("wachtwoord");
        r.setVoornaam("Thijs");
        r.setAchternaam("Berg, van der");
        r.setRechtenGroep(1);

        Gebruiker t = new Gebruiker();
        t.setEmail("info@michiel.nl");
        t.setWachtwoord("wachtwoord");
        t.setVoornaam("Michiel");
        t.setAchternaam("Kwartel, van der");
        t.setRechtenGroep(1);

        r.save();
        t.save();

        // create a couple of events...
        Session session = sessionFactory.openSession();
        session.beginTransaction();

        // now lets pull events from the database and list them
        session = sessionFactory.openSession();
        session.beginTransaction();
        List result = session.createQuery( "from Gebruiker" ).list();
        for ( Gebruiker gebruiker : (List<Gebruiker>) result ) {
            System.out.println( "Gebruiker (" + gebruiker.getVoornaam() );
        }
        session.getTransaction().commit();
        session.close();
    }

}
