package Onderwijscatalogus.TestCases;

import Onderwijscatalogus.Routers.MainRouter;
import junit.framework.TestCase;
import org.restlet.Component;
import org.restlet.Request;
import org.restlet.Response;
import org.restlet.data.Method;
import org.restlet.data.Protocol;

public class ApiModuleTest extends TestCase {

    private MainRouter mainRouter;
    private Component component;

    @Override
    public void setUp() throws Exception {
        super.setUp();

        component = new Component();
        component.getServers().add(Protocol.HTTP, 8182);
        mainRouter = new MainRouter(component);
    }

    /**
     * Test module retrieval by id
     * @throws Exception
     */
    public void testModuleGet() throws Exception {
        String urlUnderTest = "/module/1";

        Request request = new Request(Method.GET, urlUnderTest);
        Response response = new Response(request);
        mainRouter.handle(request, response);

        assertEquals(200, response.getStatus().getCode());

    }

    /**
     * Test module retrieval by id combined with books
     * @throws Exception
     */
    public void testModuleGetBooks() throws Exception {
        String urlUnderTest = "/module/1/boeken";

        Request request = new Request(Method.GET, urlUnderTest);
        Response response = new Response(request);
        mainRouter.handle(request, response);

        assertEquals(200, response.getStatus().getCode());
    }

    /**
     * Test module retrieval by list
     * @throws Exception
     */
    public void testModuleList() throws Exception {
        String urlUnderTest = "/module/list";

        Request request = new Request(Method.GET, urlUnderTest);
        Response response = new Response(request);
        mainRouter.handle(request, response);

        assertEquals(200, response.getStatus().getCode());

    }
}