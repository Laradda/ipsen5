package Onderwijscatalogus.TestCases;

import Onderwijscatalogus.Models.Boek;
import Onderwijscatalogus.Resources.ApiBoekList;
import Onderwijscatalogus.Routers.MainRouter;
import junit.framework.TestCase;
import org.restlet.Client;
import org.restlet.Component;
import org.restlet.Request;
import org.restlet.Response;
import org.restlet.data.Method;
import org.restlet.data.Protocol;

public class ApiBoekTest extends TestCase {

    private MainRouter mainRouter;
    private Component component;

    @Override
    public void setUp() throws Exception {
        super.setUp();

        component = new Component();
        component.getServers().add(Protocol.HTTP, 8182);
        mainRouter = new MainRouter(component);
    }

    /**
     * Test if a boek is fetched by ISBN under normal circumstances
     * @throws Exception
     */
    public void testBoekGetISBN() throws Exception {
        String urlUnderTest = "/boek/isbn/12345";

        Request request = new Request(Method.GET, urlUnderTest);
        Response response = new Response(request);
        mainRouter.handle(request, response);

        assertEquals(200, response.getStatus().getCode());
    }

    /**
     * Test if one boek is retrieved under normal circumstances
     * @throws Exception
     */
    public void testBoekGetId() throws Exception {
        String urlUnderTest = "/boek/1";

        Request request = new Request(Method.GET, urlUnderTest);
        Response response = new Response(request);
        mainRouter.handle(request, response);

        assertEquals(200, response.getStatus().getCode());
    }

    /**
     * Test if a list is generated under normal circumstances
     *
     * @throws Exception
     */
    public void testBoekList() throws Exception {
        String urlUnderTest = "/boek/list";

        Request request = new Request(Method.GET, urlUnderTest);
        Response response = new Response(request);
        mainRouter.handle(request, response);

        assertEquals(200, response.getStatus().getCode());
    }

}