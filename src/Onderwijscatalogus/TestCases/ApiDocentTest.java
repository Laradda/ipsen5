package Onderwijscatalogus.TestCases;

import Onderwijscatalogus.Routers.MainRouter;
import junit.framework.TestCase;
import org.restlet.Component;
import org.restlet.Request;
import org.restlet.Response;
import org.restlet.data.Method;
import org.restlet.data.Protocol;

public class ApiDocentTest extends TestCase {

    private MainRouter mainRouter;
    private Component component;

    public void setUp() throws Exception {
        super.setUp();

        component = new Component();
        component.getServers().add(Protocol.HTTP, 8182);
        mainRouter = new MainRouter(component);
    }

    /**
     * Test docent retrieval by id
     * @throws Exception
     */
    public void testDocentGet() throws Exception {
        String urlUnderTest = "/docent/1";

        Request request = new Request(Method.GET, urlUnderTest);
        Response response = new Response(request);
        mainRouter.handle(request, response);

        assertEquals(200, response.getStatus().getCode());
    }

    /**
     * Test docent retrieval combined with modules
     * @throws Exception
     */
    public void testDocentGetModules() throws Exception {
        String urlUnderTest = "/docent/1/modules";

        Request request = new Request(Method.GET, urlUnderTest);
        Response response = new Response(request);
        mainRouter.handle(request, response);

        assertEquals(200, response.getStatus().getCode());
    }

    /**
     * Test docent retrieval for full list
     * @throws Exception
     */
    public void testDocentList() throws Exception {
        String urlUnderTest = "/docent/list";

        Request request = new Request(Method.GET, urlUnderTest);
        Response response = new Response(request);
        mainRouter.handle(request, response);

        assertEquals(200, response.getStatus().getCode());

    }
}