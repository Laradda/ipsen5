package Onderwijscatalogus.TestCases; /**
 * Created with IntelliJ IDEA.
 * User: reshadfarid
 * Date: 21-05-14
 * Time: 13:17
 * To change this template use File | Settings | File Templates.
 */

import Onderwijscatalogus.Models.Boek;
import junit.framework.TestCase;

public class Boek_test extends TestCase {

    public void testBasicUsage() {

        Boek boek = new Boek();
        boek.setAuteur("Thijs van den Berg");
        boek.setBeschrijving("Dit is de beschrijving");
        boek.setIsbn("ISBN1234");
        boek.setTitel("Titel");

        boek.save();

    }

}
