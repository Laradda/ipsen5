package Onderwijscatalogus.Security;

import Onderwijscatalogus.DataAccess.DataQueries;
import Onderwijscatalogus.Models.Gebruiker;
import Onderwijscatalogus.Models.Recht;
import Onderwijscatalogus.Routers.MainRouter;
import org.joda.time.DateTime;
import org.joda.time.Seconds;
import org.restlet.Request;
import org.restlet.Response;
import org.restlet.data.ChallengeScheme;
import org.restlet.data.Form;
import org.restlet.data.Method;
import org.restlet.data.Status;
import org.restlet.security.ChallengeAuthenticator;

import java.util.ArrayList;
import java.util.List;

public class MainAuthenticator extends ChallengeAuthenticator
{
	private AuthenticationRole role;
    private MainRouter router;
	public MainAuthenticator(MainRouter router, ChallengeScheme challengeScheme,   AuthenticationRole role)
	{
		super(router.getContext(),challengeScheme,"MY_REALM");
		this.role = role;
        this.router = router;
	}

	@Override
	protected int beforeHandle(Request request, Response response)
	{
		// Check if the used Method is allowed.
		ArrayList<Method> allowedMethod = role.GetAllowedMethods();

		if (allowedMethod != null && !allowedMethod.contains(request.getMethod()))
		{
			response.setStatus(Status.CLIENT_ERROR_METHOD_NOT_ALLOWED);
			return STOP;
		}

		// Check if we need to do authorization.
		if (role.requireAuthorization())
		{
            Form variables = request.getResourceRef().getQueryAsForm();

            String api_key = variables.getFirstValue("api_key");

            int user_id = Integer.parseInt(variables.getFirstValue("user_id"));
            DataQueries dq = new DataQueries();
            Gebruiker gebruiker = dq.getGebruiker(user_id);

            //check against database api_key
            if(gebruiker == null || !gebruiker.getApikey().equals(api_key) ) {
                response.setStatus(Status.CLIENT_ERROR_UNAUTHORIZED);
                return STOP;
            }

            //check against expired key
            DateTime dt = new DateTime();
            dt.plusMinutes(10);
            DateTime cdt = new DateTime(gebruiker.getApikeycreated().replace(" ", "T"));

            Seconds seconds = Seconds.secondsBetween(cdt, dt);

            if(seconds.getSeconds() > 1800) {
                response.setStatus(Status.CLIENT_ERROR_UNAUTHORIZED);
                return STOP;
            }

            //check against ACL
            List<Recht> rechtenLijst = dq.getRechtenGroep(gebruiker.getRechtenGroep());
            List<String> lijst = new ArrayList<String>();

            for (Recht recht : rechtenLijst) {
                lijst.add(recht.getKey());
            }

            if (!lijst.contains(role.getKey())) {
                response.setStatus(Status.CLIENT_ERROR_FORBIDDEN);
                return STOP;
            }

            // save the user
            router.setUser(gebruiker);

            // do the authorization
            response.setStatus(Status.SUCCESS_OK);
            return CONTINUE;
		}

		// skip the authentication
		response.setStatus(Status.SUCCESS_OK);
		return CONTINUE;
	}
}
