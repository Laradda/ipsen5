package Onderwijscatalogus.Security;

import java.util.ArrayList;

import org.restlet.Request;
import org.restlet.data.Method;

public class AuthenticationRole
{
    private	boolean requireAuthentication;
    private	ArrayList<Method> method;
    private String key;

    public AuthenticationRole(Method method)
    {
        this.method = new ArrayList<Method>();
        this.method.add(method);
    }
    public AuthenticationRole(Method method, String key)
    {
        this(method);
        this.key = key;
        requireAuthentication = true;
    }

    public String getKey()
    {
        return this.key;
    }

	public boolean requireAuthorization()
	{
		return this.requireAuthentication;
	}

	public ArrayList<Method> GetAllowedMethods()
	{
		return this.method;
	}

}
