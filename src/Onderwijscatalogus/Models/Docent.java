package Onderwijscatalogus.Models;

import Onderwijscatalogus.DataAccess.Entity;

import java.util.ArrayList;

public class Docent extends Entity
{
	private int id, gebruiker;

	private String lokaal;

	private ArrayList<Module> modules;

	private String telefoonnummer;

	public Docent()
	{
		super();
        this.tableName = "Docent";
	}

	public int getGebruiker()
	{
		return gebruiker;
	}

	public int getId()
	{
		return id;
	}
    public void setId(int id)
    {
        this.id = id;
    }
	public String getLokaal()
	{
		return lokaal;
	}

	public ArrayList<Module> getModules()
	{
		return modules;
	}

	public String getTelefoonnummer()
	{
		return telefoonnummer;
	}

	public void setGebruiker(int gebruiker)
	{
		this.gebruiker = gebruiker;
	}

	public void setLokaal(String lokaal)
	{
		this.lokaal = lokaal;
	}

	public void setModules(ArrayList<Module> modules)
	{
		this.modules = modules;
	}

	public void setTelefoonnummer(String telefoonnummer)
	{
		this.telefoonnummer = telefoonnummer;
	}
}
