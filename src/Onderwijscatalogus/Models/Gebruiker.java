package Onderwijscatalogus.Models;

import Onderwijscatalogus.DataAccess.Entity;

public class Gebruiker extends Entity
{
	private int id, rechtenGroep;

	private String voornaam;
    private String achternaam;
    private String email;
    private String wachtwoord;
    private String apikey;
    private String apikeycreated;

    public Gebruiker()
    {
        super();
        this.tableName = "Gebruiker";
    }

	public String getAchternaam()
	{
		return achternaam;
	}

	public String getEmail()
	{
		return email;
	}

	public int getId()
	{
		return id;
	}

	public int getRechtenGroep()
	{
		return rechtenGroep;
	}

	public String getVoornaam()
	{
		return voornaam;
	}

	public String getWachtwoord()
	{
		return wachtwoord;
	}

    public String getApikey()
    {
        return apikey;
    }

    public String getApikeycreated()
    {
        return apikeycreated;
    }

    public void setId(Integer id)
    {
        this.id = id;
    }

	public void setAchternaam(String achternaam)
	{
		this.achternaam = achternaam;
	}

	public void setEmail(String email)
	{
		this.email = email;
	}

	public void setRechtenGroep(int rechtenGroep)
	{
		this.rechtenGroep = rechtenGroep;
	}

	public void setVoornaam(String voornaam)
	{
		this.voornaam = voornaam;
	}

	public void setWachtwoord(String wachtwoord)
	{
		this.wachtwoord = wachtwoord;
	}

    public void setApikey(String apikey)
    {
        this.apikey = apikey;
    }

    public void setApikeycreated(String apikeycreated)
    {
        this.apikeycreated = apikeycreated;
    }
}