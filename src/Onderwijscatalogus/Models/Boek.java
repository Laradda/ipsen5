package Onderwijscatalogus.Models;

import Onderwijscatalogus.DataAccess.Entity;

public class Boek extends Entity
{
	private String auteur;
	private String beschrijving;
	private int id;
	private String isbn;

	private String titel;


    public Boek(){
        this.tableName = "boek";
    }
	public String getAuteur()
	{
		return auteur;
	}

	public String getBeschrijving()
	{
		return beschrijving;
	}

	public int getId()
	{
		return id;
	}

    public void setId(int id){this.id = id;}

	public String getIsbn()
	{
		return isbn;
	}

	public String getTitel()
	{
		return titel;
	}

	public void setAuteur(String auteur)
	{
		this.auteur = auteur;
	}

	public void setBeschrijving(String beschrijving)
	{
		this.beschrijving = beschrijving;
	}

	public void setIsbn(String isbn)
	{
		this.isbn = isbn;
	}

	public void setTitel(String titel)
	{
		this.titel = titel;
	}
}