package Onderwijscatalogus.Models;

import Onderwijscatalogus.DataAccess.Entity;

import java.util.ArrayList;

public class Module extends Entity
{
	private String beoordeling;
	private String beschrijvingInhoudelijk;
	private String beschrijvingLeerdoelen;
	private ArrayList<Boek> boeken;
	private String code;
	private String competenties;
	private String contacturenWerkvormen;

	private String eindeisen;

	private int id;

	private String leerdoelen;

	private String leerstof;

	private String naam;

	private String status;

	private String studiefase;

	private Integer studiepunten;

	private String voorkennis;

    private Integer studieperiode;
    private String studiejaar;
    private Boolean propedeuse;
    private String verplichtVoorSpecialisatie;

    public Module(){
        this.tableName = "module";
    }
	public String getBeoordeling()
	{
		return beoordeling;
	}

	public String getBeschrijvingInhoudelijk()
	{
		return beschrijvingInhoudelijk;
	}

	public String getBeschrijvingLeerdoelen()
	{
		return beschrijvingLeerdoelen;
	}

	public ArrayList<Boek> getBoeken()
	{
		return boeken;
	}

	public String getCode()
	{
		return code;
	}

	public String getCompetenties()
	{
		return competenties;
	}

	public String getContacturenWerkvormen()
	{
		return contacturenWerkvormen;
	}

	public String getEindeisen()
	{
		return eindeisen;
	}

	public int getId()
	{
		return id;
	}

    public void setId(int id){this.id = id;}

	public String getLeerdoelen()
	{
		return leerdoelen;
	}

	public String getLeerstof()
	{
		return leerstof;
	}

	public String getNaam()
	{
		return naam;
	}

	public String getStatus()
	{
		return status;
	}

	public String getStudiefase()
	{
		return studiefase;
	}

	public Integer getStudiepunten()
	{
		return studiepunten;
	}

	public String getVoorkennis()
	{
		return voorkennis;
	}

	public void setBeoordeling(String beoordeling)
	{
		this.beoordeling = beoordeling;
	}

	public void setBeschrijvingInhoudelijk(String beschrijvingInhoudelijk)
	{
		this.beschrijvingInhoudelijk = beschrijvingInhoudelijk;
	}

	public void setBeschrijvingLeerdoelen(String beschrijvingLeerdoelen)
	{
		this.beschrijvingLeerdoelen = beschrijvingLeerdoelen;
	}

	public void setBoeken(ArrayList<Boek> boeken)
	{
		this.boeken = boeken;
	}

	public void setCode(String code)
	{
		this.code = code;
	}

	public void setCompetenties(String competenties)
	{
		this.competenties = competenties;
	}

	public void setContacturenWerkvormen(String contacturenWerkvormen)
	{
		this.contacturenWerkvormen = contacturenWerkvormen;
	}

	public void setEindeisen(String eindeisen)
	{
		this.eindeisen = eindeisen;
	}

	public void setLeerdoelen(String leerdoelen)
	{
		this.leerdoelen = leerdoelen;
	}

	public void setLeerstof(String leerstof)
	{
		this.leerstof = leerstof;
	}

	public void setNaam(String naam)
	{
		this.naam = naam;
	}

	public void setStatus(String status)
	{
		this.status = status;
	}

	public void setStudiefase(String studiefase)
	{
		this.studiefase = studiefase;
	}

	public void setStudiepunten(Integer studiepunten)
	{
		this.studiepunten = studiepunten;
	}

	public void setVoorkennis(String voorkennis)
	{
		this.voorkennis = voorkennis;
	}

    public Integer getStudieperiode() {
        return studieperiode;
    }

    public void setStudieperiode(Integer studieperiode) {
        this.studieperiode = studieperiode;
    }

    public String getStudiejaar() {
        return studiejaar;
    }

    public void setStudiejaar(String studiejaar) {
        this.studiejaar = studiejaar;
    }

    public Boolean getPropedeuse() {
        return propedeuse;
    }

    public void setPropedeuse(Boolean propedeuse) {
        this.propedeuse = propedeuse;
    }

    public String getVerplichtVoorSpecialisatie() {
        return verplichtVoorSpecialisatie;
    }

    public void setVerplichtVoorSpecialisatie(String verplichtVoorSpecialisatie) {
        this.verplichtVoorSpecialisatie = verplichtVoorSpecialisatie;
    }
}