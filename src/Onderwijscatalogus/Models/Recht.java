package Onderwijscatalogus.Models;

import Onderwijscatalogus.DataAccess.Entity;

public class Recht extends Entity
{
	private int id;
	private String key;

    public Recht()
    {
        super();
        this.tableName = "recht";
    }

	public int getId()
	{
		return id;
	}

    public void setId(int id){this.id = id;}

	public String getKey()
	{
		return key;
	}

	public void setKey(String key)
	{
		this.key = key;
	}
}