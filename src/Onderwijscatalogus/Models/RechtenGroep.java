package Onderwijscatalogus.Models;

import Onderwijscatalogus.DataAccess.Entity;

import java.util.ArrayList;

public class RechtenGroep extends Entity
{
	private int id;
	private String naam;

    public RechtenGroep(){
        this.tableName = "rechten_groep";
    }

	public int getId()
	{
		return id;
	}

    public void setId(int id){this.id = id;}

	public String getNaam()
	{
		return naam;
	}

	public void setNaam(String naam)
	{
		this.naam = naam;
	}
}