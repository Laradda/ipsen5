package Onderwijscatalogus.Models;

import Onderwijscatalogus.DataAccess.Entity;

public class RechtInGroep extends Entity
{
	private int recht_id;
	private int groep_id;
    private int id;

    public RechtInGroep(){
        this.tableName = "recht_in_groep";
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return this.id;
    }

    public void setGroep_id(int id) {
        this.groep_id = id;
    }

    public int getGroep_id() {
        return this.groep_id;
    }

	public void setRecht_id(int id)
	{
		this.recht_id = id;
	}

    public int getRecht_id() {
        return this.recht_id;
    }
}