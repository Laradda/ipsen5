package Onderwijscatalogus.DataAccess;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.Map;

/**
 * Created by Thijs on 28-5-2014.
 */
public abstract class Entity implements ExclusionStrategy, Serializable {

    private SessionFactory sessionFactory;

    protected String tableName;

    private SessionFactory getSessionFactory(){
        if(this.sessionFactory == null){
            this.sessionFactory = new Configuration()
                    .configure() // configures settings from hibernate.cfg.xml
                    .buildSessionFactory();
        }
        return this.sessionFactory;
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        if(sessionFactory != null){
            if(!sessionFactory.isClosed()) {
                sessionFactory.close();
            }
        }
    }

    public void save(){
        Session session = getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();

        session.save(this);
        transaction.commit();
        session.close();
    }

    public void update(){
        Session session = getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();

        session.update(this);
        transaction.commit();
        session.close();
    }

    public void delete(){
        Session session = getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();

        session.delete(this);
        transaction.commit();
        session.close();
    }
    public void setFields(Map<String,String> map){

        Class<?> c = getClass();

        for(String key : map.keySet()){

            // skip fields from the Entity type.
            if(key.equals("tableName") || key.equals("sessionFactory"))
                continue;

            try
            {
                String value = map.get(key);

                Field field = c.getDeclaredField(key);
                field.setAccessible(true);
                Class type = field.getType();
                field.set(this,toObject(type,value));
            }
            catch(NoSuchFieldException ex)
            {
                ex.printStackTrace();
            }
            catch(IllegalAccessException ex)
            {
                ex.printStackTrace();
            }
        }
    }


    public boolean shouldSkipClass(Class<?> c)
    {
        return false;
    }

    public boolean shouldSkipField(FieldAttributes f) {

        return f.getName().equals("tableName") || f.getName().equals("sessionFactory");
    }
    public static Object toObject( Class clazz, String value ) {
        if( Boolean.class == clazz || Boolean.TYPE == clazz) return Boolean.parseBoolean( value );
        if( Byte.class == clazz || Byte.TYPE == clazz) return Byte.parseByte( value );
        if( Short.class == clazz || Short.TYPE == clazz) return Short.parseShort( value );
        if( Integer.class == clazz || Integer.TYPE == clazz) return Integer.parseInt( value );
        if( Long.class == clazz || Long.TYPE == clazz) return Long.parseLong( value );
        if( Float.class == clazz || Float.TYPE == clazz) return Float.parseFloat( value );
        if( Double.class == clazz || Double.TYPE == clazz) return Double.parseDouble( value );
        return value;
    }
}
