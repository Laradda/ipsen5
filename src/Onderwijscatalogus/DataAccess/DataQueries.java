package Onderwijscatalogus.DataAccess;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.List;

import Onderwijscatalogus.Models.*;
import com.sun.tools.javac.util.*;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

/**
 * Created by Thijs on 16-6-2014.
 */
public class DataQueries
{
    private SessionFactory sessionFactory;

    private SessionFactory getSessionFactory(){
        if(this.sessionFactory == null){
            this.sessionFactory = new Configuration()
                    .configure() // configures settings from hibernate.cfg.xml
                    .buildSessionFactory();
        }
        return this.sessionFactory;
    }

    @Override
    protected void finalize() throws Throwable
    {
        super.finalize();
        if(sessionFactory != null){
            if(!sessionFactory.isClosed()) {
                sessionFactory.close();
            }
        }
    }

    private <T extends Entity> T getOne(Class<T> c, int id)
    {
        Session session = getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();

        Query query = session.createQuery("from "+ c.getName()+" where id = :id");
        query.setInteger("id", id);

        List list = query.list();

        transaction.commit();
        session.close();

        if(list.size() == 0)
            return null;

        return (T)list.get(0);
    }

    public Boek getBoekByIsbn(String boek)
    {

        Session session = getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();

        Query query = session.createQuery("from "+ Boek.class.getName() +" where isbn = :isbn");
        query.setString("isbn", boek);

        List list = query.list();

        transaction.commit();
        session.close();

        if(list.size() == 0)
            return null;

        return (Boek)list.get(0);
    }

    public <T extends Entity> List<T> getAll(Class<T> c)
    {
        Session session = getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();

        Query query = session.createQuery("from "+ c.getName());

        List<T> entityList = new ArrayList<T>();
        List list = query.list();
        for(Object o : list){
            entityList.add((T)o);
        }

        transaction.commit();
        session.close();

        return entityList;
    }

    public <T extends Entity> List<T> getRechtenByRechtenGroep(Class<T> c, int rechtengroep_id) {
        Session session = getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();

        System.out.println(rechtengroep_id);

        Query query = session.createQuery("select r.id, r.key from "+c.getName()+" as r, "+RechtInGroep.class.getName()+" as rig where r.id = rig.recht_id and rig.groep_id = :rechtengroep_id");
        query.setInteger("rechtengroep_id", rechtengroep_id);

        List<T> entityList = new ArrayList<T>();
        List list = query.list();

        for (Iterator it = list.iterator(); it.hasNext(); ) {
            Object[] myResult = (Object[]) it.next();
            int id = (Integer) myResult[0];
            String key = (String) myResult[1];

            Recht r = new Recht();
            r.setId(id);
            r.setKey(key);
            entityList.add((T) r);
        }

        transaction.commit();
        session.close();

        return entityList;
    }

    public List<Integer> getModulesByDocent(int docentID){
        Session session = getSessionFactory().openSession();

        Transaction transaction = session.beginTransaction();




        Query query = session.createSQLQuery("select h.module_id from docent_heeft_module h where h.docent_id = :docent_id");
        query.setInteger("docent_id", docentID);


        List<Integer> entityList = new ArrayList<Integer>();
        List list = query.list();
        for(Object o : list){
            entityList.add((Integer)o);
        }
        transaction.commit();
        session.close();

        return entityList;
    }

    public boolean apiKeyExpired(String api_key, int user_id) {
        return false;
    }

    public Module getModule(int id) {
        return getOne(Module.class, id);
    }

    public Docent getDocent(int id) {
        return getOne(Docent.class, id);
    }

    public List<Module> GetModules() {
        return getAll(Module.class);
    }

    public Gebruiker getGebruiker(int id) {
        return getOne(Gebruiker.class, id);
    }

    public List<Recht> getRechtenGroep(int id) {
        return getRechtenByRechtenGroep(Recht.class, id);
    }

    public RechtInGroep getGroepRecht(int id) {
        return getOne(RechtInGroep.class, id);
    }

    public Recht getRecht(int id) {
        return getOne(Recht.class, id);
    }

    public RechtenGroep getGroep(int id) {
        return getOne(RechtenGroep.class, id);
    }

    public Boek getBoek(int id) {
        return getOne(Boek.class, id);
    }

    public String generateKey(String email, String password)
    {
        Session session = getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();

        Query query = session.createQuery("from Gebruiker where email = :email and wachtwoord = :wachtwoord");
        query.setString("email", email);
        query.setString("wachtwoord", password);

        List list = query.list();

        transaction.commit();
        session.close();


        if(list.size() == 0)
            return null; // invalid login

        Gebruiker gebruiker = (Gebruiker)list.get(0);

        String key = new BigInteger(65,new SecureRandom()).toString();

        gebruiker.setApikey(key);
        SimpleDateFormat format = new SimpleDateFormat();
        format.applyPattern("yyyy-MM-dd HH:mm:ss");

        gebruiker.setApikeycreated(format.format(new java.util.Date()));

        gebruiker.update();

        return key;

    }

    public List<Integer> getModuleBooks(int moduleID) {
        Session session = getSessionFactory().openSession();

        Transaction transaction = session.beginTransaction();




        Query query = session.createSQLQuery("select h.boek_id from module_heeft_boek h where h.module_id = :module_id");
        query.setInteger("module_id", moduleID);


        List<Integer> entityList = new ArrayList<Integer>();
        List list = query.list();
        for(Object o : list){
            entityList.add((Integer)o);
        }
        transaction.commit();
        session.close();

        return entityList;

    }

    public int addBoekToModule(int module_id, int boek_id)
    {

        Session session = getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();

        Query query = session.createSQLQuery("insert into module_heeft_boek set module_id = :module_id, boek_id = :boek_id");
        query.setInteger("module_id", module_id);
        query.setInteger("boek_id", boek_id);
        int r = query.executeUpdate();

        transaction.commit();
        session.close();

        return r;
    }

    public int removeBoekFromModule(int module_id, int boek_id)
    {

        Session session = getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();

        Query query = session.createSQLQuery("DELETE FROM module_heeft_boek WHERE module_id = :module_id AND boek_id = :boek_id");
        query.setInteger("module_id", module_id);
        query.setInteger("boek_id", boek_id);
        int r = query.executeUpdate();

        transaction.commit();
        session.close();

        return r;
    }
}