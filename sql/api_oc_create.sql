SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema api_oc
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `api_oc` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `api_oc` ;

-- -----------------------------------------------------
-- Table `api_oc`.`rechten_groep`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `api_oc`.`rechten_groep` (
  `groep_id` INT NOT NULL AUTO_INCREMENT,
  `groep_naam` VARCHAR(45) NULL,
  PRIMARY KEY (`groep_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `api_oc`.`gebruiker`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `api_oc`.`gebruiker` (
  `gebruiker_id` INT NOT NULL AUTO_INCREMENT,
  `email` VARCHAR(150) NULL,
  `wachtwoord` VARCHAR(255) NULL,
  `voornaam` VARCHAR(50) NULL,
  `achternaam` VARCHAR(100) NULL,
  `groep_id` INT NOT NULL,
  PRIMARY KEY (`gebruiker_id`),
  INDEX `fk_gebruiker_rechten_groep1_idx` (`groep_id` ASC),
  CONSTRAINT `fk_gebruiker_rechten_groep1`
    FOREIGN KEY (`groep_id`)
    REFERENCES `api_oc`.`rechten_groep` (`groep_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `api_oc`.`docent`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `api_oc`.`docent` (
  `docent_id` INT NOT NULL AUTO_INCREMENT,
  `telefoonnummer` VARCHAR(20) NULL,
  `lokaal` VARCHAR(20) NULL,
  `gebruiker_id` INT NOT NULL,
  PRIMARY KEY (`docent_id`),
  INDEX `fk_docent_gebruiker_idx` (`gebruiker_id` ASC),
  CONSTRAINT `fk_docent_gebruiker`
    FOREIGN KEY (`gebruiker_id`)
    REFERENCES `api_oc`.`gebruiker` (`gebruiker_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `api_oc`.`boek`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `api_oc`.`boek` (
  `boek_id` INT NOT NULL,
  `titel` VARCHAR(255) NULL,
  `isbn` VARCHAR(100) NULL,
  `auteur` VARCHAR(200) NULL,
  `beschrijving` TEXT NULL,
  PRIMARY KEY (`boek_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `api_oc`.`module`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `api_oc`.`module` (
  `module_id` INT NOT NULL AUTO_INCREMENT,
  `naam` VARCHAR(200) NULL,
  `code` VARCHAR(20) NULL,
  `studiepunten` INT NULL,
  `studiefase` TEXT NULL,
  `status` TEXT NULL,
  `leerdoelen` TEXT NULL,
  `voorkennis` TEXT NULL,
  `competenties` TEXT NULL,
  `beschrijving_leerdoelen` TEXT NULL,
  `beschrijving_inhoudelijk` TEXT NULL,
  `eindeisen` TEXT NULL,
  `contacturen_werkvormen` TEXT NULL,
  `beoordeling` TEXT NULL,
  `leerstof` TEXT NULL,
  PRIMARY KEY (`module_id`),
  UNIQUE INDEX `code_UNIQUE` (`code` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `api_oc`.`docent_heeft_module`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `api_oc`.`docent_heeft_module` (
  `docent_id` INT NOT NULL,
  `module_id` INT NOT NULL,
  INDEX `fk_docent_heeft_module_docent1_idx` (`docent_id` ASC),
  INDEX `fk_docent_heeft_module_module1_idx` (`module_id` ASC),
  CONSTRAINT `fk_docent_heeft_module_docent1`
    FOREIGN KEY (`docent_id`)
    REFERENCES `api_oc`.`docent` (`docent_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_docent_heeft_module_module1`
    FOREIGN KEY (`module_id`)
    REFERENCES `api_oc`.`module` (`module_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `api_oc`.`module_heeft_boek`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `api_oc`.`module_heeft_boek` (
  `module_id` INT NOT NULL,
  `boek_id` INT NOT NULL,
  INDEX `fk_module_heeft_boek_module1_idx` (`module_id` ASC),
  INDEX `fk_module_heeft_boek_boek1_idx` (`boek_id` ASC),
  CONSTRAINT `fk_module_heeft_boek_module1`
    FOREIGN KEY (`module_id`)
    REFERENCES `api_oc`.`module` (`module_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_module_heeft_boek_boek1`
    FOREIGN KEY (`boek_id`)
    REFERENCES `api_oc`.`boek` (`boek_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `api_oc`.`recht`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `api_oc`.`recht` (
  `recht_id` INT NOT NULL AUTO_INCREMENT,
  `key` VARCHAR(100) NULL,
  PRIMARY KEY (`recht_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `api_oc`.`recht_in_groep`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `api_oc`.`recht_in_groep` (
  `groep_id` INT NOT NULL,
  `recht_id` INT NOT NULL,
  INDEX `fk_recht_in_groep_rechten_groep1_idx` (`groep_id` ASC),
  INDEX `fk_recht_in_groep_recht1_idx` (`recht_id` ASC),
  CONSTRAINT `fk_recht_in_groep_rechten_groep1`
    FOREIGN KEY (`groep_id`)
    REFERENCES `api_oc`.`rechten_groep` (`groep_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_recht_in_groep_recht1`
    FOREIGN KEY (`recht_id`)
    REFERENCES `api_oc`.`recht` (`recht_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
