ALTER TABLE `gebruiker` ADD `api_key` TEXT NULL , ADD `api_key_created` TIMESTAMP NULL ;

ALTER TABLE `module` ADD `studieperiode` INT NULL , ADD `studiejaar` TEXT NULL , ADD `propedeuse` BIT(1) NULL , ADD `verplicht_voor_specialisatie` TEXT NULL ;